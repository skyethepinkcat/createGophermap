#!/bin/bash

# Starting text, definetly change this!
gophermapTxt="!Skye\'s Phlog\n\n"

printf "$gophermapTxt" > gophermap
EXT="txt"
SORT="ls -l"
HELPTXT="FALSE";
while getopts "s:f:" option;
do
	case "${option}" in
		s)
			SORT="${OPTARG}"
			;;
		f)
			EXT=".${OPTARG}"
			;;
		h)
			HELPTXT="TRUE";
	esac
done

if [[ "$HELPTXT" == "FALSE" ]];
then
	# Find all .txt files and organize them by last modified
	find . -name "*$EXT" -print0 | xargs -0 $SORT | while read file
	do
		# Get the first line
		line1="$(head -n 1 $file)"
		# Get the second line
		line2="$(head -n 2 $file | tail -n 1)"
		# Add the relevant lines to the gophermap
		printf "0$line1 $line2 	${file:2}\n" >> gophermap
	done;
else
	printf "Use -s to give a sorting method. Use -f to give a file extension. The defaults are ls -t and txt.\n"
fi
