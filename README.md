# Create Gophermap Script

An easy script to create a gophermap from a bunch of text files. The filing format is required for the script to work correctly presently:

```
<Title>
<Date>
<
Body
>
```
Note, the script sorts by last modified by default, rather than the given date. This may be changed at a later date.

Arguments [default]:
	-s	Give it a ls command to sort with [ls -l]
	-f	Change the file extension [txt]
